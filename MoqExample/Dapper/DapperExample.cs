﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MoqExample.Entity;

namespace MoqExample.Dapper
{
    public class DapperExample
    {
        public List<Clientes> PesquisaCliente()
        {
            var clientes = new List<Clientes>();

            using(var conexao =  new SqlConnection())
            {
                clientes = conexao.Query<Clientes>("Select * from Clientes").ToList();
            }
            return clientes;
        }
    }
}
