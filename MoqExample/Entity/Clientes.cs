﻿using System;

namespace MoqExample.Entity
{
    public class Clientes
    {
        public Clientes(int codigo, string nome, DateTime dataNascimento)
        {
            Codigo = codigo;
            Nome = nome;
            DataNascimento = dataNascimento;
        }
        public int Codigo { get; private set; }
        public string Nome { get; private set; }
        public DateTime DataNascimento { get; private set; }
    }
}
