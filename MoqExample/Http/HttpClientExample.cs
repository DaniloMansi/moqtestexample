﻿using MoqExample.Entity;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MoqExample.Http
{
    public class HttpClientExample
    {
        private readonly HttpClient _httpClient;

        public HttpClientExample(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<List<Clientes>> GetClients()
        {
            var requestUri = "http://api.client.com/clients";
            var response = await _httpClient.GetAsync(requestUri);
            var responseData = await response.Content.ReadAsStringAsync();
            var clients = JsonConvert.DeserializeObject<List<Clientes>>(responseData);

            return clients;
        }
    }
}
