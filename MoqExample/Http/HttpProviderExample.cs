﻿using MoqExample.Entity;
using MoqExample.Interface;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace MoqExample.Http
{
    public class HttpProviderExample
    {
        
        private readonly IHttpProvider _httpProvider;

        public HttpProviderExample(IHttpProvider httpProvider)
        {
            _httpProvider = httpProvider;
        }

        
        public async Task<List<Clientes>> GetClients()
        {
            var requestUri = "http://api.client.com/clients";
            var response = await _httpProvider.GetAsync(requestUri);
            var responseData = await response.Content.ReadAsStringAsync();
            var clients = JsonConvert.DeserializeObject<List<Clientes>>(responseData);

            return clients;
        }

        public async Task PostPrivateMethod()
        {
            await Post();
        }

        public async Task DeleteProtectedMethod()
        {
            await Delete();
        }

        private async Task Post()
        {
            var clients = new List<Clientes>()
            {
                new Clientes(2,"Nome2", new System.DateTime(1995,08,14))
            };
            var requestUri = "http://api.client.com/clients";
            var byteContent = System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(clients));
            var content = new ByteArrayContent(byteContent);
            await _httpProvider.PostAsync(requestUri, content);
        }

        protected async Task Delete()
        {
            var requestUri = "http://api.client.com/clients";
            await _httpProvider.DeleteAsync(requestUri);
        }
    }
}
