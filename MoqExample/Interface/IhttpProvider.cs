﻿using System.Net.Http;
using System.Threading.Tasks;

namespace MoqExample.Interface
{
    public interface IHttpProvider
    {
        Task<HttpResponseMessage> GetAsync(string request);
        Task<HttpResponseMessage> PostAsync(string request, HttpContent content);
        Task<HttpResponseMessage> PutAsync(string request, HttpContent content);
        Task<HttpResponseMessage> DeleteAsync(string request);

    }
}
