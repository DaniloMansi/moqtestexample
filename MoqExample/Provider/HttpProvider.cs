﻿using MoqExample.Interface;
using System.Net.Http;
using System.Threading.Tasks;

namespace MoqExample.Provider
{
    public class HttpProvider : IHttpProvider
    {
        private readonly HttpClient _httpClient;

        public HttpProvider(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public Task<HttpResponseMessage> DeleteAsync(string request) => _httpClient.DeleteAsync(request);

        public Task<HttpResponseMessage> GetAsync(string request) => _httpClient.GetAsync(request);

        public Task<HttpResponseMessage> PostAsync(string request, HttpContent content) => _httpClient.PostAsync(request, content);

        public Task<HttpResponseMessage> PutAsync(string request, HttpContent content) => _httpClient.PutAsync(request, content);
    }
}
