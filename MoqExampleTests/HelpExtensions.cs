﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MoqExampleTests
{
    public static class HelpExtensions
    {
        public static bool ByProperties<T>(this T expected, T compare) where T : class
        {
            var type = typeof(T);
            var confirm = true;
            foreach (System.Reflection.PropertyInfo pi in type.GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance))
            {
                if (pi.Name == "Item")
                {
                    confirm = type.GetProperty(pi.Name).GetValue(expected, null).ByProperties(type.GetProperty(pi.Name).GetValue(compare, null));
                }
                else
                {
                    object selfValue = type.GetProperty(pi.Name).GetValue(expected, null);
                    object toValue = type.GetProperty(pi.Name).GetValue(compare, null);

                    if (selfValue != toValue && (selfValue == null || !selfValue.Equals(toValue)))
                    {
                        confirm = false;
                    }
                }
            }
            return confirm;
        }
    }
}
