﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using MoqExample.Entity;
using MoqExample.Http;
using MoqExample.Interface;
using FluentAssertions;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;


namespace MoqExample.Tests
{
    [TestClass()]
    public class HttpProviderExampleTests
    {
        [TestMethod()]
        public async Task CallApiClientTest()
        {
            //Arrange
            var clients = new List<Clientes>()
            {
                new Clientes(1,"Nome",new System.DateTime(1990,03,01))
            };
            var httpResponse = new HttpResponseMessage();
            var byteContent = System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(clients));
            httpResponse.Content = new ByteArrayContent(byteContent);
            httpResponse.StatusCode = System.Net.HttpStatusCode.Accepted;
            var mockHttpProvider = new Mock<IHttpProvider>();

            mockHttpProvider.Setup(set => set.GetAsync(It.IsAny<string>())).ReturnsAsync(httpResponse);
            //mockHttpProvider.Setup(set => set.GetAsync("http://api.client.com/clients")).ReturnsAsync(httpResponse);


            var httpProviderTest = new HttpProviderExample(mockHttpProvider.Object);

            //Act
            var response = await httpProviderTest.GetClients();

            //Assert
            Assert.IsTrue(response[0].Nome == clients[0].Nome);
        }

        [TestMethod()]
        public async Task CallSendHttpClient()
        {
            //Arrange 
            var mockHandler = new Mock<HttpMessageHandlerOverride>();
            mockHandler.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", It.IsAny<HttpRequestMessage>(), It.IsAny<CancellationToken>()).ReturnsAsync(new HttpResponseMessage(System.Net.HttpStatusCode.Accepted));
            var httpTest = new HttpClient(mockHandler.Object);
            await httpTest.SendAsync(new HttpRequestMessage());
        }

        [TestMethod()]
        public async Task CallHttpClientTest()
        {
            //Arrange
            var clients = new List<Clientes>()
            {
                new Clientes(1,"Nome",new System.DateTime(1990,03,01))
            };

            var httpResponse = new HttpResponseMessage();
            var byteContent = System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(clients));
            httpResponse.Content = new ByteArrayContent(byteContent);

            httpResponse.StatusCode = System.Net.HttpStatusCode.Accepted;

            var mockHandler = new Mock<HttpMessageHandlerOverride>();
            mockHandler.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>()).ReturnsAsync(httpResponse);

            var httpTest = new HttpClient(mockHandler.Object);

            HttpClientExample clientExample = new HttpClientExample(httpTest);

            //Act
            var response = await clientExample.GetClients();
            response.Should().BeEquivalentTo(clients);
            response.Should().HaveCountLessThan(3, "Limite maximo de 3 itens por lista").And.NotBeEmpty($"Resposta nao pode ser nulo");
        }
    }
}